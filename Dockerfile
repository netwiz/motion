FROM alpine AS build
MAINTAINER Steven Haigh <netwiz@crc.id.au>

# Setup build environment
RUN apk add --no-cache \
    autoconf automake build-base ffmpeg-dev gettext-dev git libjpeg-turbo-dev libmicrohttpd-dev pkgconfig

ADD 1442.patch /1442.patch

RUN git clone --depth 1 --branch 4.4 https://github.com/Motion-Project/motion.git && \
   cd motion  && \
   sed -i 's/0.19/0.21/g' configure.ac && \
   patch -p1 -i /1442.patch && \
   autoreconf -fiv && \
   ./configure && \
   make clean && \
   make -j$(nproc) && \
   make install && \
   cd .. && \
   rm -fr motion

FROM alpine

# Setup Timezone packages and avoid all interaction. This will be overwritten by the user when selecting TZ in the run command
RUN apk add --no-cache \
    curl ffmpeg libjpeg-turbo libmicrohttpd openssh-client rsync

# Setup parameters
ARG BUILD_DATE
ARG VCS_REF
LABEL org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.docker.dockerfile="Dockerfile" \
    org.label-schema.license="GPLv3" \
    org.label-schema.name="motion" \
    org.label-schema.url="https://motion-project.github.io/" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://github.com/Motion-Project/motion.git"

# Copy build binary
COPY --from=build /usr/local /usr/local

# R/W needed for motion to update configurations
VOLUME /usr/local/etc/motion
# R/W needed for motion to update Video & images
VOLUME /var/lib/motion

CMD test -e /usr/local/etc/motion/motion.conf || \
    cp /usr/local/etc/motion/motion-dist.conf /usr/local/etc/motion/motion.conf

CMD [ "motion", "-n" ]
